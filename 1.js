function createList(array, parent = document.body) {
    const list = document.createElement('ul'); // Створюємо елемент списку
    array.forEach(item => {
      const listItem = document.createElement('li'); // Створюємо елемент списку
      listItem.textContent = item;   // Встановлюємо текстовий вміст елементу списку
      list.appendChild(listItem); // Додаємо елемент списку до батьківського елемента (за замовчуванням document.body)
    });
    parent.appendChild(list);
  }

  const array1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
  createList(array1);
  const a = 5;
  const b = 4;
  const sum = a+b;
  const array2 = ["1", "2", "3", sum, "user", 23];
  createList(array2);
